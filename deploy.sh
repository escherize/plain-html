cd blog \
    && bundle exec jekyll build \
    && rm -rf ../public/blog \
    && mv _site ../public/blog \
    && cd .. \
    && git add -A && git commit -am "deploy-auto-commit" && git push && \
    echo "Complete - go check the site."
